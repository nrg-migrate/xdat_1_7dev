package org.nrg.xft.utils.ValidationUtils;

public interface ValidationResultsI {

	/**
	 * If there were any erros then false, else true.
	 * @return
	 */
	public abstract boolean isValid();

}