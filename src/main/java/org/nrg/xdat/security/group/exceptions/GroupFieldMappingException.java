package org.nrg.xdat.security.group.exceptions;

public class GroupFieldMappingException extends Exception {
	public GroupFieldMappingException(Exception e){
		super(e);
	}
}
