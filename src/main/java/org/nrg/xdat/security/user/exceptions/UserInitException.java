package org.nrg.xdat.security.user.exceptions;

public class UserInitException extends Exception {

    public UserInitException(String message) {
        super(message);
    }
}